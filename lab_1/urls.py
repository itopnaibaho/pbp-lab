from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('friends/', views.friend_list, name= 'friend_list'),
    
    # TODO Add friends path using friend_list Views
]
