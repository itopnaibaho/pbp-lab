from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('note-list', views.note_list, name = 'note-list'),
    path('add-note', views.add_note, name = 'add-note')

]