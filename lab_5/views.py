from django.shortcuts import render
from lab_2.models import Note

# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'note' :note}
    return render(request, 'lab2.html', response)

def get_note(request, id):
    GNote = Note.objects.all()