from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add', views.add_friend)
    # TODO Add friends path using friend_list Views
]
