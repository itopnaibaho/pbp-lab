from django import forms
from . import models

class FriendForm(forms.ModelForm):
    class Meta:
        model = models.Friend
        fields = "__all__"
        
