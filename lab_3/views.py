from django.shortcuts import render
from datetime import datetime, date
from . import models
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

mhs_name = 'Christopher Moses Nathanael'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001, 8, 2)  # TODO Implement this, format (Year, Month, Date)
npm = 2006597361  # TODO Implement this
# Create your views here.
@login_required(login_url ='/admin/login/')
def index(request):
    friends = models.Friend.objects.all()
    response = {"friends":friends}
    return render(request, 'lab3_index.html', response)
            
@login_required(login_url ='/admin/login/')
def add_friend(request):
    text = {}
    form = FriendForm(request.POST or None)
    if(form.is_valid and request.method == 'POST'):
        form.save()
    text['form'] = form
    return render(request, "lab3_form.html",text)    
