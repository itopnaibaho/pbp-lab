from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('xml', views.xml, name= 'xml'),
    path('json', views.json, name = 'json')
    # TODO Add friends path using friend_list Views
]
